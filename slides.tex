\documentclass{beamer}
\usetheme{metropolis}           % Use metropolis theme
\title{git gud}
\subtitle{A guide to all things version control}
\date{\today}
\author{Michael Lydeamore}
\newcommand{\git}{\texttt{git}}
\newcommand{\gitt}{\texttt{git} }
\setbeamercolor{block title alerted}{use=structure, bg=black!10}
\setbeamercolor{block body alerted}{use=structure, bg=black!5}

\usepackage{tcolorbox}

\usepackage{tikz}
\usetikzlibrary{arrows}
\usetikzlibrary{decorations.markings}
\tikzset{
o/.style={
shorten >=#1,
decoration={
markings,
mark={
at position 1
with {
\draw[fill=black] circle [radius=#1];
}
}
},
postaction=decorate
},
o/.default=2pt
}
\usetikzlibrary{calc}

\tikzstyle{int}=[draw, minimum size=2em]
\tikzstyle{init} = [pin edge={to-,thin,black}]
\tikzset{treatment/.style={dashed}}

\newenvironment{spacimize} { 
     \begin{itemize}
     \setlength\itemsep{2em}
} {
     \end{itemize}
}
\newenvironment{spacerate} { 
     \begin{enumerate}
     \setlength\itemsep{1em}
} {
     \end{enumerate}
}

\tcbset{%
    noparskip,
    colback=gray!10, %background color of the box
    colframe=gray!40, %color of frame and title background
    coltext=black, %color of body text
    coltitle=black, %color of title text 
    fonttitle=\bfseries,
    alerted/.style={coltitle=red, 
                     colframe=gray!40},
    example/.style={coltitle=black, 
                     colframe=green!20,             
                     colback=green!5},
    }


\begin{document}
  \maketitle

  \begin{frame}{Overview}
  	\begin{spacerate}
  		\item What is \git?
  		\item Version control in 5 commands or less!
  		\item An introduction to branching
  	\end{spacerate}
  \end{frame}

  \begin{frame}{\git: The global information tracker}
    \begin{spacimize}
    	\item \gitt is a \alert{version control system}.
    	\item At it's core, \gitt is entirely local.
    		\begin{itemize}
    			\item But most often used in conjunction with online platforms.
    		\end{itemize}
    	\item The `operations' of \gitt form a mathematical group.
    \end{spacimize}
  \end{frame}

  \begin{frame}{We must go deeper!}
  	A single \gitt project is known as a \alert{repository}.

  	Online storage of a repository is known as a \alert{remote}.

  	Repository versions are known as \alert{commits}.
  	\begin{itemize}
   		\item Each commit contains all the changes that have happened since the last commit.
  		\item Each commit is identified by a unique sequence of characters.
  	\end{itemize}
  	Because \gitt is reversible, knowing the initial state of the repository, and the commits means that the \emph{entire} history of the repository is known.
  \end{frame}

  \begin{frame}{\texttt{Git}-ting started}
  To start a new \gitt repository, simply type:
  	
  	\begin{tcolorbox}[title=Command]
  		\texttt{git init}
  	\end{tcolorbox}

  \end{frame}

  \begin{frame}{\texttt{Git}-ting up where someone else left off}
  If you already have an online repository, you probably want to \alert{clone} it.
  \vspace{5mm}
  \begin{tcolorbox}[title=Command]
  	\texttt{git clone https://path.to.repo/repositoryname.git}
  \end{tcolorbox}
  \vspace{5mm}
  Bitbucket and GitHub have different path formats, so be sure to check!
  \end{frame}

  \begin{frame}{Try \gitt now}
  Let's clone our first repository. It's these slides!

  \vspace{3mm}

  \begin{tcolorbox}[title=Command]
  {\footnotesize\texttt{git clone} 

  \texttt{https://mlydeamore@bitbucket.org/mlydeamore/git-session.git}}
  \end{tcolorbox}

  \vspace{5mm}

  Replace the first \texttt{mlydeamore} with your username!
  \end{frame}

  \begin{frame}{Ch-ch-changes}
  Whenever you change something in a repository, you register this change via a \alert{commit}.

  Much like writing, \alert{commit early, commit often}.

  \vspace{5mm}
  There are two stages to committing:
  \begin{spacerate}
  	\item \texttt{add}, which tells \gitt what files you want to register the change and,
  	\item \texttt{commit}, which tells \gitt that you are ready to store these changes.
  \end{spacerate}

  \end{frame}

  \begin{frame}{Don't be a \texttt{commit}-ment phobe!}
  The `lazy' commit simply adds all files \gitt is already looking at.
  \begin{tcolorbox}[title=Command]
  \texttt{git commit -am "Your message here"}
  \end{tcolorbox}
  \vspace{5mm}
  Note: this will \alert{not} add any newly created files, nor delete any already existing files.
  \end{frame}

  \begin{frame}{Be \texttt{push}-y}
  Committing only stages your changes \alert{locally}.

  For updating the \alert{remote}, one must \texttt{push} the commits.

  \begin{tcolorbox}[title=Command]
  	\texttt{git push}
  \end{tcolorbox}

  \end{frame}

  \begin{frame}{For every action, there's an equal and opposite reaction}
  If your working copy is missing some \texttt{commit}s from the remote, the \texttt{commit}s can be \alert{\texttt{pull}}\texttt{ed} down.

  \begin{tcolorbox}[title=Command]
  	\texttt{git pull}
  \end{tcolorbox}

  \end{frame}
  
  \begin{frame}{Marriage Counselling}

  If you have made changes that have not been staged, there may be a \alert{conflict}. There is no catch-all solution for these, although the simplest is to discard one end of the changes using,

  \begin{tcolorbox}[title=Command]
  	\texttt{git stash}
  \end{tcolorbox}	
  \end{frame}

  \begin{frame}{The story so far}

  \begin{tcolorbox}[title=Command]
  	\texttt{git init}
  	
  	\texttt{git clone}
  	
  	\texttt{git commit}
  	
  	\texttt{git push}
  	
  	\texttt{git pull}

  	\texttt{git stash}
  \end{tcolorbox}

  \end{frame}

  \section{But there's more...}

  \begin{frame}{What did the tree do when it wanted to code a new feature?}
  It created a new \alert{\texttt{branch}}.

  To think about branching, one needs to think about \gitt in a tree-like way.

  \begin{center}
  \begin{tikzpicture}[node distance=1.5cm,auto,>=latex']
  \node[int] (C1) {$C_1$};
  \node[int] (C2) [right of=C1] {$C_2$};
  \node[int] (C3) [right of=C2] {$C_3$};
  \node[int] (C4) [right of=C3] {$C_4$};

  \node[int] (B2) [below of=C2] {$B_2$};
  \node[int] (B3) [right of=B2] {$B_3$};

  \node (master) at ($(C1)+(0, 0.7)$) {\texttt{master}};

  \draw[->] (C1) to (C2);
  \draw[->] (C2) to (C3);
  \draw[->] (C3) to (C4);

  \path[->] (C2) edge node[left] {\texttt{branch}} (B2);
  \draw[->] (B2) to (B3);
  \end{tikzpicture}
  \end{center}

  \end{frame}

  \begin{frame}{A \texttt{branch} a day keeps the doctor away!}
  	 For `proper' \gitt flow, every new feature should be in it's own branch to reduce conflicts. Branches are easy to create.

  	 \begin{tcolorbox}[title=Command]
  	 \texttt{git branch branchname}
  	 \end{tcolorbox}

  	 To switch to a different \texttt{branch} (including new ones!), it can be checked out using,

  	 \begin{tcolorbox}[title=Command]
  	 \texttt{git checkout branchname}
  	 \end{tcolorbox}

  	 Checking out goes a little further, and one can actually \texttt{checkout} any commit!
  \end{frame}

  \begin{frame}{Combining commands can be nice}
  	 Both of the creation of a new branch and the checking out can be merged together using

  	 \begin{tcolorbox}[title=Command]
  	 \texttt{git checkout -b branchname}
  	 \end{tcolorbox}
  \end{frame}

  \begin{frame}{Reunited at last}
  	When a branch has served it's purpose, it can be merged back into \texttt{master} using

  	\begin{tcolorbox}[title=Command]
  	\texttt{git merge branchname}
  	\end{tcolorbox}

  \end{frame}

  \begin{frame}{\gitt \texttt{fork}-ed}
  	If you want to work on someone elses code, but don't have full access to their repository, you can make your own copy, known as a \alert{\texttt{fork}}.

  	A fork is essentially a branch, but only you have access to it.

  	Unlike the local \gitt commands, forking is usually done through the web interface.

  	\vspace{5mm}

  	Create a \alert{\texttt{fork}} of this repository and clone it to your computer.
  \end{frame}

  \begin{frame}{A \texttt{fork} is a repository by any other name...}
  	When you \texttt{fork} a repository, you get the entire history of it, but are able to make any changes you see fit. For example, you could modify this slide to say \alert{\texttt{banana}} and no-one could stop you!

  	But the changes are only \texttt{push}ed to your fork, not to the main repository.

  	If you have some changes you'd like to end up in the main repository, you could create a \alert{pull request}. Like forking, a pull request is usually done through the web hosting of the repository.
  \end{frame}

  \begin{frame}{Recap}
	\begin{tcolorbox}[title=Command]
		\texttt{git branch}

		\texttt{git checkout}

		\texttt{git merge}
	\end{tcolorbox}  

	Also \texttt{fork}ing.
  \end{frame}

  \begin{frame}{Just the \texttt{head} of the iceberg}
  	This is not an exhaustive list of \gitt commands.

  	With an understanding of the \gitt language, the rest of the commands can be understood.

  	There are many more advanced topics and workflows to make your \gitt experience a better one.
  \end{frame}

\end{document}